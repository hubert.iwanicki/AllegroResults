package JoeMonsterPackage.pages;

import JoeMonsterPackage.utils.Details;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by Ja on 06.02.2017.
 */
public class GalleryPage {

    protected WebDriver driver;
    private static final String NAV_TO_SEARCH ="//li[@class='menuJMSelected']/ul/li/a[text()='Szukaj']";
    private static final String SEARCH_QUERY = "(//div/form/input[@name='q'])[2]";
    private static final String SEARCH_BUTTON ="//button";
    private static final String TITLES ="//div/a/b";
    private static final String LIKES ="//b/img[@src='/images/mikro-ok.png']";
    private static final String ADDED_TIME = "//b/span/span";


    @FindBy(xpath=NAV_TO_SEARCH)
    protected WebElement navToSearchPage;

    @FindBy(xpath=SEARCH_QUERY)
    protected WebElement searchQueryInput;

    @FindBy(xpath=SEARCH_BUTTON)
    protected WebElement searchButton;

    public GalleryPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void searchWinterContent(String winter) throws InterruptedException{
        validateDisplayedItems(navToSearchPage);
        navToSearchPage.click();
        waitForElement();
        searchQueryInput.clear();
        searchQueryInput.sendKeys(winter);
        searchButton.click();
        waitForElement();
    }
    public void getQueryResultsAndSave() throws IOException{
        Details det = new Details();
        Map<String,Details> items = new HashMap<String,Details>();
        List<WebElement> titl = driver.findElements(By.xpath(TITLES));
        List<WebElement> likes = driver.findElements(By.xpath(LIKES));
        List<WebElement> addedTime = driver.findElements(By.xpath(ADDED_TIME));
        List<String> titlStr;
        FileWriter writer = new FileWriter("./topLiked.txt");
        for(int counter=0; counter<10; counter++){
            addedTime.add(addedTime.get(counter));
            det.setAddedTime(addedTime.get(counter).getText());
            likes.add(likes.get(counter));
            det.setLikesCounter(likes.get(counter).getText());
            items.put(titl.get(counter).getText(),det);
            writer.write(titl.get(counter).getText()+" - ");
            writer.write(addedTime.get(counter).getText());
            writer.append(System.getProperty("line.separator"));
            System.out.println(titl.get(counter).getText());
            System.out.println(addedTime.get(counter).getText());
            System.out.println(likes.get(counter).getText());
        }
        writer.close();
        //System.out.print(items.get(titl.get(0)));
        //System.out.print(items.get(titl.get(5)));
    }

    public void validateDisplayedItems(WebElement element){
        assertTrue("New item filter isn't displayed! ", element.isDisplayed());
        assertTrue("New item filter isn't enabled! ", element.isEnabled());
    }
    public void waitForElement() throws InterruptedException {
        Thread.sleep(2000);
    }
}
