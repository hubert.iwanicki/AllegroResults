package JoeMonsterPackage.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by Ja on 06.02.2017.
 */
public class HomePage {

    protected WebDriver driver;
    private static final String NAV_TO_GALLERY = "//li[@class='menuJMunselected']/a[text()='Galeria']";

    @FindBy(xpath=NAV_TO_GALLERY)
    protected WebElement navToGalleryButton;

    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void open(String url){
        driver.get(url);
    }
    public void goToGallery(){
        validateDisplayedItems(navToGalleryButton);
        navToGalleryButton.click();
    }
    public void validateDisplayedItems(WebElement element){
        assertTrue("New item filter isn't displayed! ", element.isDisplayed());
        assertTrue("New item filter isn't enabled! ", element.isEnabled());
    }
}
