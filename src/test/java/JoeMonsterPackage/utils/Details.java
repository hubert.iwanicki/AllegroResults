package JoeMonsterPackage.utils;

public class Details {

    protected String details;
    protected String likesCounter;
    protected String viewsCounter;
    protected String addedTime;

    public String getDetails(){
         return details;
    }
    public void setDetails(String details){
        this.details = details;
    }
    public String getLikesCounter(){
        return likesCounter;
    }
    public void setLikesCounter(String likesCounter){
        this.likesCounter = likesCounter;
    }
    public String getViewsCounter(){
        return viewsCounter;
    }
    public void setViewsCounter(String viewsCounter){
        this.viewsCounter = viewsCounter;
    }
    public String getAddedTime(){
        return addedTime;
    }
    public void setAddedTime(String addedTime){
        this.addedTime = addedTime;
    }
}