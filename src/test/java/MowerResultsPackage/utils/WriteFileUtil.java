package MowerResultsPackage.utils;

import org.openqa.selenium.WebElement;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.List;

public class WriteFileUtil {

    public void fillXmlFile(List<WebElement> listProductName, List<WebElement> listPrice,
                            List<WebElement> listPriceWithDelivery,List<WebElement> listUtilityState ,List<WebElement> listImageUrl) {
        try {
        File file = new File("./mowerResults.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(ItemsDatabaseList.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ItemsDatabaseList items = new ItemsDatabaseList();
        for(int counter = 0; counter < 10; counter++)
        {
            Item itemSingle = new Item();
            listProductName.add(listProductName.get(counter));
            itemSingle.setProductName(getContentFromElement(listProductName,counter));
            listPrice.add(listPrice.get(counter));
            itemSingle.setPrice(getContentFromElement(listPrice,counter));
            listPriceWithDelivery.add(listPriceWithDelivery.get(counter));
            itemSingle.setPriceWithDelivery(getContentFromElement(listPriceWithDelivery,counter));
            listUtilityState.add(listUtilityState.get(counter));
            itemSingle.setUtilityState(getContentFromElement(listUtilityState,counter));
            listImageUrl.add(listImageUrl.get(counter));
            itemSingle.setImageUrl(listImageUrl.get(counter).getAttribute("data-src"));
            items.getItems().add(itemSingle);
        }
            jaxbMarshaller.marshal(items, file);
            jaxbMarshaller.marshal(items, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
    public String getContentFromElement(List<WebElement> list, int count){
        return list.get(count).getText();
    }
}